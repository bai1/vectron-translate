package com.danielrharris.vectrontranslate;

import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class TranslationThread extends Thread {
    private static final Set<TMessage> toGo = Collections.newSetFromMap(new ConcurrentHashMap());
  
    public static void schedule(String player, String message) {
        TMessage toAdd = new TMessage();
        toAdd.message = message;
        toAdd.player = player;
        synchronized (toGo) {
            toGo.add(toAdd);
        }
    }
  
    @Override
    public void run() {
        while (!isInterrupted()) {
            synchronized (toGo) {
                for (final TMessage tm : toGo) {
                    Language senderLang1;
                    Player player = Bukkit.getServer().getPlayer(tm.player);
                    if(!(player instanceof Player)) {
                        senderLang1 = Language.ENGLISH;
                    } else {
                        senderLang1 = (Language)VectronTranslate.known.get(((Player) player).getUniqueId());
                    }
                    final Language senderLang = senderLang1;
                    
                    // Console output support
                    try {
                        String msgPost = Translate.execute(tm.message, senderLang, Language.ENGLISH);
                        if(!(msgPost.equalsIgnoreCase(tm.message))) {
                            if(msgPost.toLowerCase().contains("ArgumentOutOfRangeException".toLowerCase())) {
                                // nothing
                            } else {
                                System.out.println(ChatColor.YELLOW + tm.player + " (From " + senderLang.toString().toUpperCase() + "): " + ChatColor.AQUA + msgPost);
                            }
                        }
                    } catch (Exception ex) {
                    
                    }
                
                    Set<Language> needs = new HashSet();
                        for (Player target1 : Bukkit.getOnlinePlayers()) {
                            if (!target1.getName().equals(tm.player)) {
                                if (target1.hasPermission("vectron.translate")) {
                                    Language targetLan1 = (Language)VectronTranslate.known.get(target1.getUniqueId());
                                    if (targetLan1 != Language.AUTO_DETECT) {
                                        needs.add(targetLan1);
                                    }
                                }
                            }
                        }
                    final Map<Language, String> translated = new HashMap();
                    for (Language targetLan1 : needs) {
                        if (targetLan1 != null) {
                            try {
                                translated.put(targetLan1, Translate.execute(tm.message, senderLang, targetLan1));
                            } catch (Exception ex) {
                                if(ex.toString().contains("OAuth2-13")) {
                        
                                } else if (ex.getCause()!=null) {
                                    if(ex.getCause().toString().contains("UnknownHost")) {
                                    
                                    }
                                } else {
                                    Logger.getLogger(VectronTranslate.class.getName()).log(Level.SEVERE, null, ex);
                                    Logger.getLogger(VectronTranslate.class.getName()).log(Level.SEVERE, "targetLang: " + targetLan1);
                                    Logger.getLogger(VectronTranslate.class.getName()).log(Level.SEVERE, "senderLang: " + senderLang);
                                    Logger.getLogger(VectronTranslate.class.getName()).log(Level.SEVERE, "Message: " + tm.message);
                                }
                            }
                        }
                    }
                    Bukkit.getScheduler().scheduleSyncDelayedTask(VectronTranslate.i, new Runnable() {
                        public void run() {
                            for (Player target : Bukkit.getOnlinePlayers()) {
                                if (!target.getName().equals(tm.player)) {
                                    if (target.hasPermission("vectron.translate")) {
                                        Language targetLang = (Language)VectronTranslate.known.get(target.getUniqueId());
                                        if (targetLang != Language.AUTO_DETECT && targetLang != senderLang) {
                                            String msgPost = (String)translated.get(targetLang);
                                            if(!(msgPost==null)) {
                                                if (!(msgPost.equalsIgnoreCase(tm.message))) {
                                                    if(!(msgPost.contains("credentials has zero balance"))) {
                                                        target.sendMessage(ChatColor.YELLOW + tm.player + " (From " + senderLang.toString().toUpperCase() + "): " + ChatColor.AQUA + msgPost);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }, 1L);
                }
                toGo.clear();
                }
        
            try {
                Thread.sleep(5L);
            } catch (InterruptedException ex) {
            
            }
        }
    }
  
    private static class TMessage {
        private String player;
        private String message;
    
        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof TMessage)) {
                return false;
            }
            
            TMessage tobj = (TMessage)obj;
            return (tobj.player.equals(this.player)) && (tobj.message.equals(this.message));
        }
    
        @Override
        public int hashCode() {
            int hash = 7;
            return hash;
        }
    }
}