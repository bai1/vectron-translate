package com.danielrharris.vectrontranslate;

import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.SayCommand;
import org.bukkit.entity.Player;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class VectronTranslate extends JavaPlugin implements Listener {
    public static Map<UUID, Language> known = new ConcurrentHashMap();
    public static VectronTranslate i;
    public static boolean configured;

    @Override
    public void onEnable() {
        getDataFolder().mkdir();
        File ff = new File(getDataFolder(), "vt.dat");
        try {
            if (ff.exists()) {
                known = (Map)SLAPI.load(ff.getPath());
            } else {
                ff.createNewFile();
                known = (Map)SLAPI.load(ff.getPath());
            }
        } catch (Exception ex) {
            Logger.getLogger(VectronTranslate.class.getName()).log(Level.SEVERE, null, ex);
        }

        i = this;
        PluginManager pm = getServer().getPluginManager();
        getConfig().addDefault("Client-ID", "clientid");
        getConfig().addDefault("Client-Secret", "secreykey");
        getConfig().addDefault("No-permission", "&cError: No permission!");
        getConfig().options().copyDefaults(true);
        saveConfig();
        String clientid = getConfig().getString("Client-ID");
        String secretkey = getConfig().getString("Client-Secret");
        Translate.setClientId(getConfig().getString("Client-ID"));
        Translate.setClientSecret(getConfig().getString("Client-Secret"));

        // Validation checking
        if(clientid==""||clientid=="clientid"||clientid==null||secretkey==""||secretkey=="secretkey"||secretkey==null) {
            getLogger().severe("Bing API Client ID or Secret Key is missing or unedited from config.yml");
            getLogger().severe("Resolve this by editing /plugins/VectronTranslate/config.yml");
            configured = false;
            getLogger().info("Configuration check failed.");
        } else {
            getLogger().info("Configuration check successful.");
            getLogger().info("Validating Client-ID and Secret-key.");
            String configTest = "unas gatos";
            try {
                configTest = Translate.execute("unas gatos", Language.ENGLISH);
            } catch (Exception ex) {
                //Nothing here for now
            }
            if(!(configTest.equalsIgnoreCase("some cats"))) {
                getLogger().severe("Validation test failed. Make sure your Client-ID and Secret-key are correct");
                configured = false;
            } else {
                getLogger().info("Validation test successful.");
                configured = true;
            }
        }

        pm.registerEvents(this, this);
        new TranslationThread().start();
        if(configured=false) {
            getLogger().info("VectronTranslate encountered errors while enabling. Fix these before properly using the plugin.");
        } else {
            getLogger().info("Vectron translate enabled successfully");
        }
    }

    @Override
    public void onDisable() {
        File ff = new File(getDataFolder(), "vt.dat");
        try {
            if (ff.exists()) {
                ff.delete();
            }
            ff.createNewFile();
            SLAPI.save(known, ff.getPath());
        } catch (Exception ex) {
            Logger.getLogger(VectronTranslate.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void noPermission(CommandSender sender) {
        String noPerm = getConfig().getString("No-permission");
        String noPermAct = noPerm.replace("&", "\u00A7");
        sender.sendMessage(noPermAct);
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Language senderLang1;
        if(!(sender instanceof Player)) {
            senderLang1 = Language.ENGLISH;
        } else {
            senderLang1 = (Language)known.get(((Player) sender).getUniqueId());
        }
        final Language senderLang = senderLang1;
        
        if(command.getName().equalsIgnoreCase("say")) {
            if(!(sender.hasPermission("bukkit.command.say"))) {
                noPermission(sender);
            } else {
                if (args.length == 0)  {
                    sender.sendMessage(ChatColor.RED + "Usage: /say <message...>");
                    return true;
                }
                final SayCommand SayCommand = new SayCommand();
                SayCommand.execute(sender, label, args);
                StringBuilder bmessage = new StringBuilder();

                if (args.length > 0) {
                    bmessage.append(args[0]);
                    for (int i = 1; i < args.length; i++) {
                        bmessage.append(" ").append(args[i]);
                    }
                }
                String msgPre = bmessage.toString();
                String sender1 = sender.getName();
                runTranslation(msgPre, sender1);
            }
            return true;
        }
        
        if(command.getName().equalsIgnoreCase("getlang")) {
            if(!(sender.hasPermission("vectron.getlanguage"))) {
                return true;
            }
            if(args.length == 0) {
                return false;
            }
            
            Player target = Bukkit.getPlayer(args[0]);
            Language targetLang;
            
            if(target==null) {
                sender.sendMessage(ChatColor.AQUA + args[0] + ChatColor.GREEN + " is not online.");
                return true;
            } else {
                targetLang = (Language)known.get(target.getUniqueId());
            }

            if(targetLang == null || targetLang == Language.AUTO_DETECT) {
                sender.sendMessage(ChatColor.GREEN + "Language of " + ChatColor.AQUA + target.getName() + ChatColor.GREEN + " is not set. They may not have played here.");
            } else {
                sender.sendMessage(ChatColor.GREEN + "Language of " + ChatColor.AQUA + target.getName() + ChatColor.GREEN + " is " + ChatColor.AQUA + targetLang.toString().toUpperCase());
            }
        }
        
        if(command.getName().equalsIgnoreCase("vectron")) {
            if(sender.hasPermission("vectron.reload")) {
                if(!(args.length == 0)) {
                    if(args[0].equalsIgnoreCase("reload")) {
                        this.reloadConfig();
                        Translate.setClientId(getConfig().getString("Client-ID"));
                        Translate.setClientSecret(getConfig().getString("Client-Secret"));
                        if(getConfig().getString("Client-ID") == "" || getConfig().getString("Client-Secret") == "" || getConfig().getString("Client-ID") == "clientid" || getConfig().getString("Client-Secret") == "secretkey" || getConfig().getString("Client-ID") == null || getConfig().getString("Client-Secret") == null) {
                            getLogger().severe("Bing API Client ID or Client Secret is missing from config.yml");
                            getLogger().severe("Resolve this by editing /plugins/VectronTranslate/config.yml");
                            getLogger().severe("Configuration check failed.");
                            configured = false;
                        } else {
                            getLogger().info("Configuration check successful.");
                            getLogger().info("Validating Client-ID and Secret-key.");
                            String configTest = "unas gatos";
                            try {
                                configTest = Translate.execute("unas gatos", Language.ENGLISH);
                            } catch (Exception ex) {
                                //Nothing here for now
                            }
                            if(!(configTest.equalsIgnoreCase("some cats"))) {
                                getLogger().severe("Validation test failed. Make sure your Client-ID and Secret-key are correct");
                                configured = false;
                            } else {
                                getLogger().info("Validation test successful.");
                                configured = true;
                            }
                        }
                        getLogger().info("Configuration reloaded.");
                        if(sender instanceof Player) {
                            sender.sendMessage("[VectronTranslate] Configuration reloaded");
                        }
                        return true;
                    }
                } else {
                sender.sendMessage(ChatColor.RED + "Usage: /vectron reload");
                }
            } else {
                noPermission(sender);
                return true;
            }
        }
        
        if(command.getName().equalsIgnoreCase("languages")) {
            if (sender.hasPermission("vectron.languages")) {
                String msgPre1 = "The following languages are supported:";
                String msgPre2 = "Arabic, " + "Bulgarian, " + "Catalan, " + 
                    "Chinese_simplified, " + "Chinese_traditional, " + 
                    "Czech, " + "Danish, " + "Dutch, " + "English, " + 
                    "Estonian, " + "Finnish, " + "French, " + 
                    "German, " + "Greek, " + "Haitian_Creole, " +
                    "Hebrew, " + "Hindi, " + "Hmong_Daw, " +
                    "Hungarian, " + "Indonesian, " +
                    "Italian, " + "Japanese, " + "Korean, " + "Latvian, " + 
                    "Lithuanian, " + "Norwegian, " + "Polish, " +
                    "Portuguese, " + "Romanian, " + "Russian, " +
                    "Slovak, " + "Slovenian, " + "Spanish, " +
                    "Swedish, " + "Thai, " + "Turkish, " +
                    "Ukrainian, " + "Vietnamese. ";
                try {
                    String msgPost1 = Translate.execute(msgPre1, senderLang);
                    String msgPost2 = Translate.execute(msgPre2, senderLang);
                    if(msgPre1.equalsIgnoreCase(msgPost1) && msgPre2.equalsIgnoreCase(msgPost2)) {
                        sender.sendMessage(ChatColor.GREEN + msgPre1);
                        sender.sendMessage(ChatColor.AQUA + msgPre2);
                    } else {
                        sender.sendMessage(ChatColor.GREEN + msgPost1);
                        sender.sendMessage(ChatColor.AQUA + msgPost2);
                    }
                } catch (Exception ex) {
                    if(ex.toString().contains("OAuth2-13")) {
                        //
                    } else if (ex.getCause() != null) {
                        if(ex.getCause().toString().contains("UnknownHost")) {
                            //
                        }
                    } else {
                        getLogger().info("Player: " + sender.getName());
                        getLogger().info("Message: COMMAND: " + command);
                        getLogger().info("Exception: " + ex);
                    }
                }
                return true;
            } else {
                noPermission(sender);
            }
        }
        
        if(command.getName().equalsIgnoreCase("translate")) {
            if (!sender.hasPermission("vectron.translate")) {
                sender.sendMessage(ChatColor.RED + "You're not allowed to do that!");
                return true;
            }
            
            if (args.length == 0) {
                sender.sendMessage(ChatColor.GREEN + "You current language is " + ChatColor.AQUA + ((Language)known.get(((Player) sender).getUniqueId())).toString().toUpperCase());
                return true;
            }
    
            String ll = args[0];
            
            if(ll.equalsIgnoreCase("help")) {
                sender.sendMessage(ChatColor.GOLD + "----- Vectron Translate Help -----");
                sender.sendMessage(ChatColor.AQUA + "/translate " + ChatColor.WHITE + "| " + ChatColor.GREEN + "Displays current state of translations.");
                sender.sendMessage(ChatColor.AQUA + "/translate [lang]" + ChatColor.WHITE + "| " + ChatColor.GREEN + "Changes your language to [lang]");
                sender.sendMessage(ChatColor.AQUA + "/translate text " + ChatColor.WHITE + "| " + ChatColor.GREEN + "Translate given text to your language.");
                sender.sendMessage(ChatColor.AQUA + "/translate off " + ChatColor.WHITE + "| " + ChatColor.GREEN + "Turns off visible chat translations.");
                sender.sendMessage(ChatColor.AQUA + "/languages " + ChatColor.WHITE + "| " + ChatColor.GREEN + "Displays known supported languages.");
                sender.sendMessage(ChatColor.AQUA + "/getlang " + ChatColor.WHITE + "| " + ChatColor.GREEN + "DIsplays the language of the specified user.");
                return true;
            }
                
            if(ll.equalsIgnoreCase("text")) {
                StringBuilder bmessage = new StringBuilder();
                
                if (args.length > 0) {
                    bmessage.append(args[0]);
                    for (int i = 1; i < args.length; i++) {
                        bmessage.append(" ").append(args[i]);
                    }
                }
                String msgPreAlt = bmessage.toString();
       
                if(msgPreAlt.equalsIgnoreCase("text")||msgPreAlt.equalsIgnoreCase("text ")) {
                    sender.sendMessage(ChatColor.RED + "ERROR: Nothing could be translated as nothing was entered");
                    return true;
                }
                    
                String msgPre = msgPreAlt.substring(5);
                try {
                String msgPost = Translate.execute(msgPre, senderLang);
                    if (msgPost.toLowerCase().contains("ArgumentOutOfRangeException".toLowerCase())) {
                        sender.sendMessage(ChatColor.RED + "ERROR: Could not translate string!");
                    } else {
                        if(msgPre.equalsIgnoreCase(msgPost)) {
                            sender.sendMessage(ChatColor.BLUE + "Result: " + ChatColor.GREEN + msgPre);
                        } else {
                            sender.sendMessage(ChatColor.BLUE + "Result: " + ChatColor.GREEN + msgPost);
                        }
                    }
                } catch (Exception ex) {
                    if(ex.toString().contains("OAuth2-13")) {
                        //
                    } else if (ex.getCause().toString() != null) {
                        if(ex.getCause().toString().contains("UnknownHost")) {
                            //
                        }
                    } else {
                        // Old error handling
                        getLogger().severe("Player: " + sender.getName());
                        getLogger().severe("Message: COMMAND: " + command + "( /" + command.getName() + ")");
                        getLogger().severe("Exception: " + ex);
                    }
                }
                return true;
            }

            if (ll.equalsIgnoreCase("off")) {
                known.put(((Player) sender).getUniqueId(), Language.AUTO_DETECT);
                sender.sendMessage(ChatColor.GREEN + "Local translations disabled");
                return true;
            }
        
            try {
                Language lee = Language.valueOf(ll.toUpperCase());
                known.put(((Player) sender).getUniqueId(), lee);
                sender.sendMessage(ChatColor.GREEN + Translate.execute(new StringBuilder().append("Your language was changed to ").append(lee.toString().toUpperCase()).toString(), Language.ENGLISH, lee));
                System.out.println(ChatColor.AQUA + sender.getName() + ChatColor.GREEN + " changed their language to " + ChatColor.AQUA + ll + ChatColor.GREEN + ".");
            } catch (Exception ex) {
                if(ex.toString().contains("OAuth2-13")) {
                    
                } else if (ex.getCause()!=null) {
                    if(ex.getCause().toString().contains("UnknownHost")) {
                        
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + ll + " is not a supported language.");
                    sender.sendMessage(ChatColor.RED + "If this is clearly an error, ensure the plugin is configured correctly.");
                    System.out.println(ChatColor.AQUA + sender.getName() + ChatColor.GREEN + " could not change their language to " + ChatColor.AQUA + ll+ ChatColor.GREEN + ".");
                }
            }
            return true;
        }
        return true;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        if (!known.containsKey(event.getPlayer().getUniqueId())) {
        known.put(event.getPlayer().getUniqueId(), Language.ENGLISH);
        }
    }
    
    public void runTranslation(String message1, String player1) {
        final String message = message1;
        final String player = player1;
        
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
            public void run() {
            TranslationThread.schedule(player, message);
            }
        }, 1L);
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        final String message = event.getMessage();
        final String player = event.getPlayer().getName();
        
        if (!(event.getPlayer().hasPermission("vectron.translate"))) {
            return;
        }
        
        runTranslation(message,player);
    }
}
